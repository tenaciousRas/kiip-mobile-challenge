# The Kiip Mobile Challenge

The challenge has 2 tasks to accomplish.

1. Create a Static Library/Framework for iOS 7 and above or Android KitKat and above. The sdk or library should support two methods `initSDK` and `showHackerNews`(https://github.com/HackerNews/API), any app developer should able to drag n drop this library to their project (.a or .framework).
 
 * `initSDK` - We can use this method to initialize any api keys or pre-cache any data if required. (No ui and network calls shouldn’t block app lifecycle)
 
 * `showHackerNews` - Should show modal view with top 20 list of hacker news articles (title, author, score, time, no. of comments) and on click of any article it should open/navigate to the url of the article (like in-app web brower using UIWebview) 

2. What is a Cordova plugin? A plugin is a bit of add-on code that provides JavaScript interface to native components/library.
 
 * One of our customer is using PhoneGap/Cordova(https://cordova.apache.org/) for the app development. As a SDK provider we need to support phonegap developer by creating a wrapper/plugin. Our PhoneGap/Cordova plugin should support above methods and functionality.

* Submit to us via Bitbucket and email careers[at]kiip.me us about it.
