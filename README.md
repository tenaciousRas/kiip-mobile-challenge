This is the master repository for the Kiip Mobile Challenge, Parts 1 & 2.  The challenge is described at https://gist.github.com/sraj/3766a23e41f47fdd712f66f852603cc2 and copied here as challenge.md.

To clone the repository you must also get the submodules.  The easiest way is to use the following command:

git clone --recursive git@bitbucket.org:tenaciousRas/kiip-mobile-challenge.git
